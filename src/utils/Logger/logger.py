import json
import time
from datetime import datetime

class Logger(object):
    def __init__(self,filename:str,Frequncy:None,**objects:any) -> None:
        self.objects = objects
        self.file = open(filename,"w", encoding="utf-8")
        self.readingNum = 0
        self.data = {}
        self.freq = Frequncy or 30.0

    def log(self) -> None:
        self.readingNum += 1
        curRead = {"time":datetime.now()}
        for name, object in self.objects.items():
            curRead[name] = vars(object) # for future replace with object.function.__dict__
        self.data[self.readingNum] = curRead

    def saveData(self) -> None:
        json.dump(self.data, self.file)
    
    def periodic(self) -> None:
        time.sleep(1/self.freq)
        self.log()