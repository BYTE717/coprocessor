from typing import List
from threading import Thread
from multiprocessing import Process, Pool

def initializeThreads(*funcs) -> List[Thread]:
    return [Thread(target=i, args=()) for i in funcs]

def executeThreads(threads:List[Thread]) -> None:
    for i in threads: i.start()
    for i in threads: i.join()

def initializeProcesses(*funcs) -> List[Process]:
    return [Process(target=i, args=()) for i in funcs]

def executeProcesses(processes:List[Process]) -> None:
    for i in processes: i.start()
    for i in processes: i.join()

def poolProcess(*funcs,Async=False) -> List[any]:
    '''
    Executes functions in parallel using mp
    @param *funcs are functions passed to run in parallel
    @param Async is a bool describing if the function is asyncronous
    if Async is true, then it just runs the functions without waiting to go on
    otherwise it waits for all processes to be completed
    '''
    with Pool(len(funcs)) as p:
        res = [p.apply_async(f) if Async else p.apply(f) for f in funcs]
        return res