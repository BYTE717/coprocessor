import threading
import ntcore
import json 
from math import pi
from typing import Tuple, List
import time

# fed paper imports + utils
from core.aim import Trajectory
from core.locate import Module
from core.locate import Triangulate as trig
from utils.paraUtils import executeProcesses, initializeProcesses
from utils.Logger import Logger
from core.taskScheduler import Task, TaskGroup, Optimizer
from core.DynamicPath import ShortestPath
from core.RpiComs import Sender, Listener

# settings
settings = json.loads(open("assets/config.json","r+"))
TEAM = 2554
VELOCITY,MINANGLE,MAXANGLE,DRAG = settings["trajectory"]["velocity"],settings["trajectory"]["minAngleRads"],settings["trajectory"]["maxAngleRads"],settings["trajectory"]["deceleration"]
CAMPORTS, FOV, CAMDISTS, PIXELWIDTH = settings["locate"]["CamPorts"],settings["locate"]["FOV"]*pi/180, settings["locate"]["DistanceBetweenCamsMeters"], settings["locate"]["PixelWidth"]
# networkTables
inst = ntcore.NetworkTableInstance.getDefault()
table = inst.getTable("datatable")
# values to write
robotX = table.getDouble("RobotX").publish()
robotY = table.getDouble("RobotY").publish()
angle = table.getDouble("angl").publish()


inst.startClient4("Pi4")
inst.setServerTeam(TEAM)
inst.startDSClient()

# intitialization of stuff

t = Trajectory(velocity=VELOCITY,lowAngle=MINANGLE,highAngle=MAXANGLE,airResistance=DRAG)

front, back, left, right = Module(CAMPORTS[0],CAMPORTS[1], CAMDISTS, FOV, PIXELWIDTH), Module(CAMPORTS[2],CAMPORTS[3], CAMDISTS, FOV, PIXELWIDTH), Module(CAMPORTS[4],CAMPORTS[5], CAMDISTS, FOV, PIXELWIDTH), Module(CAMPORTS[6],CAMPORTS[7], CAMDISTS, FOV, PIXELWIDTH)
posMath = trig()

# Log = Logger("state.json",None, trajectory=t, posMaths=posMath,frontModule=front, backModule=back, leftModule=left, rightModule=right)


def calculateDistances(robotX, robotY) -> Tuple[float,float]:
    pass

def getPos() -> Tuple[float,float]:
    allTags = trig.unique([front.getTags() + back.getTags() + left.getTags() + right.getTags()])
    return posMath.findPos(allTags)

def updateAngle() -> None:
    X,Y = getPos()
    horizontalDist, verticalDist = calculateDistances(X,Y)
    angle.set(t.optimalAngle(horizontalDist,verticalDist))
    

def updatePos() -> None:
    X,Y = getPos()
    robotX.set(X)
    robotY.set(Y)

# def fastloop() -> None:
#     executeProcesses(initializeProcesses(updateAngle, updatePos,Log.periodic))

tasks = [updateAngle, updatePos]

def main() -> None:
    ignore = [-1.0]*len(tasks)
    while True:
        try:
            t = initializeProcesses(*tasks)
            for i, p in enumerate(t):
                try:
                    if ignore[i] != -1 and time.time() - ignore[i] < 1.0:
                        continue
                    p.start()
                except Exception as e:
                    print(e)
                    ignore[i] = time.time()

            for i, p in enumerate(t):
                p.join()
                
        except Exception as e:
            print(e)
            # Log.saveData()
        
    

if __name__ == '__main__':
    main()