from .Module import Module
from .posMath import Triangulate
from .structs import tag, detected_Object

class Unit(object):
    def __init__(self, left:Module, right:Module, front:Module, back:Module) -> None:
        self.left = left
        self.right = right
        self.front = front
        self.back = back
        self.trig = Triangulate()
        
    def getPosition(self):
        allTags = Triangulate.unique([self.front.getTags() + self.back.getTags() + self.left.getTags() + self.right.getTags()])
        return self.trig.findPos(allTags)
