'''
 Copyright (c) 2024 Dhairya Sarvaiya @Byte717

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

2. Any works created using this Software or any modified version of this Software must include attribution to the original software author(s). Attribution should include the following information: 
    a. The name of the software.

    b. The name of the original author(s) of the software.

    c. The URL or other reference to the software (Github).

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''
from math import  pi, cos, sin,sqrt
from typing import List,Tuple, Dict
from structs import tag
from collections import deque

class Triangulate(object):
    default = { # 16 tags on the field
            1 : (0,0),
            2 : (0,0),
            3 : (0,0),
            4 : (0,0),
            5 : (0,0),
            6 : (0,0),
            7 : (0,0),
            8 : (0,0),
            9 : (0,0),
            10 : (0,0),
            11 : (0,0),
            12 : (0,0),
            13 : (0,0),
            14 : (0,0),
            15 : (0,0),
            16 : (0,0)
        }
    def __init__(self,map:Dict[int,Tuple[float,float]]=None, initialPose:Tuple[float,float]) -> None:
        self.tagPos = Triangulate.default or map
        self.posHistory = deque(maxlen=5e4)

    def triangulate(self,tag1:tag, tag2:tag, tag3:tag) -> Tuple[float,float]:
        x1, y1, r1 = self.tagPos[tag1.id][0], self.tagPos[tag1.id][1], tag1.distance
        x2, y2, r2 = self.tagPos[tag2.id][0], self.tagPos[tag2.id][1], tag2.distance
        x3, y3, r3 = self.tagPos[tag3.id][0], self.tagPos[tag3.id][1], tag3.distance

        yCoordinate = ((x2-x3)*((x2**2 - x1**2)+(y2**2 - y1**2)+(r1**2 - r2**2)) - (x1-x2)*((x3**2 - x2**2)+(y3**2 - y2**2)+(r2**2 - r3**2)))/(2*((y1-y2)*(x2-x3) - (y2-y3)*(x1-x2))) # brainrot
        xCoordinate = ((y2-y3)*((y2**2 - y1**2)+(x2**2 - x1**2)+(r1**2 - r2**2)) - (y1-y2)*((y3**2 - y2**2)+(x3**2 - x2**2)+(r2**2 - r3**2)))/(2*((x1-x2)*(y2-y3) - (x2-x3)*(y1-y2))) # more brainrot

        return (abs(xCoordinate),abs(yCoordinate))
    
    def findPos(self, detectedTags:List[tag]) -> Tuple[float, float] | None:
        '''
        @param detectedTags is a list of unique tags detected by the 4 modules
        returns the position of the robot
        '''
        if len(detectedTags) < 3:
            lastPt, secondLastPt = self.posHistory[-1], self.posHistory[-2]
            dy, dx = lastPt[1] - secondLastPt[1], lastPt[0] - secondLastPt[0]
            # tbd
            return None
        retX,retY,iters = 0,0,0
        for i in range(len(detectedTags)-2):
            for j in range(i+1, len(detectedTags)-1):
                for k in range(j+1, len(detectedTags)):
                    coord = self.triangulate(detectedTags[i],detectedTags[j],detectedTags[k])
                    retX, retY += coord[0],coord[1]
                    iters += 1
        self.posHistory.append((retX/iters, retY/iters))
        return self.posHistory[-1]
    
    @staticmethod
    def unique(lst:List[tag]) -> List[tag]:
        idSeen,ret = set(),[]
        for i in lst:
            if i.id not in idSeen:
                idSeen.add(i.id)
                ret.append(i)
        return ret