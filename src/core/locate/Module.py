'''
 Copyright (c) 2024 Dhairya Sarvaiya @Byte717

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

2. Any works created using this Software or any modified version of this Software must include attribution to the original software author(s). Attribution should include the following information: 
    a. The name of the software.

    b. The name of the original author(s) of the software.

    c. The URL or other reference to the software (Github).

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''
import cv2
import cv2.aruco as aruco
from math import  pi, cos, sin,sqrt
from typing import List,Tuple
from structs import tag, detected_Object, detected_Entity, entity

class Module(object):

    def __init__(self, left:int, right:int, distance:float, fov:float, screenW:int, offset:float=0) -> None:
        '''
        @param left is an integer with the port of the left camera relative to behind the module
        @param Right is an integer with the port of the left camera relative to behind the module
        @param Distance is a distance between the 2 cameras measured from the center of the sensor
        @param fov is a field of view of each camera in radians
        @param ScreenW is a width of the screen in pixel 
        @param Offset is for tuning
        '''
        self.left = left # params
        self.right = right
        self.dist = distance
        self.fov = fov
        self.width = screenW
        self.offset = offset
        self.halfD = self.dist/2

        self.dictionary = aruco.getPredefinedDictionary(aruco.DICT_APRILTAG_36h11) # works
        self.parameters =  aruco.DetectorParameters()
        self.detector = aruco.ArucoDetector(self.dictionary, self.parameters)

    def getTags(self) -> List[tag]:
        '''
        Returns a list of tag objects(structs.py)
        think of each tag object as a circle whose distance is provided
        then throw the list into the triangulate class that does the math and you have a robot position
        '''
        leftFrame, rightFrame = self.getFrame()
        leftTags,rightTags = self.detect(leftFrame,True),self.detect(rightFrame,False)
        commonTags = set([i.id for i in leftTags]) | set([i.id for i in rightTags])
        processPairs = [(j,i) for j in leftTags for i in rightTags if (i.id == j.id and i.id in commonTags)]
        ret = [tag(id=left.id, distance=self.calcDist(left.angle, right.angle)) for (left, right) in processPairs]
        return ret
        
    def getObjects(self):
        leftFrame, rightFrame = self.getFrame()
        pass

    def calcDist(self, leftAngle, rightAngle) -> float:
        alpha = pi - leftAngle - rightAngle
        s2 = (self.dist*sin(rightAngle))/sin(alpha)
        xSquared = ((self.halfD)*(self.halfD))+ (s2*s2) + (s2*self.dist)*cos(leftAngle)
        return sqrt(xSquared)

    def calcAngle(self, objectCenter:Tuple[int,int], left:bool) -> float:
        center = self.screenW/2
        d = abs(center - objectCenter[0])
        thetaPrime = (self.fov*d)/(2*self.screenW)
        toLeftOfCenter = objectCenter[0] < center
        return pi/2+ (-thetaPrime if left != toLeftOfCenter else thetaPrime)

    def detect(self, frame, left:bool) -> None | List[detected_Object]:
        corners, ids, _ = self.detector.detectMarkers(frame)

        if len(corners) < 0:
            return None

        ids = ids.flatten()
        ret = []
        for (markerCorner, markerID) in zip(corners, ids):
            (topLeft, topRight, bottomRight, bottomLeft) = markerCorner.reshape((4, 2))
            topRight = (int(topRight[0]), int(topRight[1]))
            bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
            bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
            topLeft = (int(topLeft[0]), int(topLeft[1]))
            c = self.center(topRight, bottomLeft)
            angle = self.calcAngle(c,left)
            ret.append(detected_Object(topLeft=topLeft, topRight=topRight,bottomRight=bottomRight, bottomLeft=bottomLeft,angle=angle,id=markerID))
        return ret

    def detectObjects(self,frame, left:bool):
        pass
    
    def center(self,topRight,bottomLeft):
        return ((topRight[0] + bottomLeft[0])/2,(topRight[1]+bottomLeft[1])/2)

    def getFrame(self):
        return (self.left.read()[1], self.right.read()[1])

    def getLeftFrame(self):
        return self.left.read()[1]
    
    def getRightFrame(self):
        return self.right.read()[1]

