from typing import Tuple
from dataclasses import dataclass

@dataclass
class tag:
    id:int
    distance:float

@dataclass
class detected_Object:
    topLeft:Tuple
    topRight:Tuple
    bottomRight:Tuple
    bottomLeft:Tuple
    angle:float
    id:int

@dataclass
class entity:
    id:int
    x:float
    y:float

@dataclass
class detected_Entity:
    id:int
    dist:float
    theta:float