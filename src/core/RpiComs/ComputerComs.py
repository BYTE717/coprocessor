import serial
# if we run this on a computer, we might need to talk and listen to pi/arduino through serial
class SerialComs(object):
    def __init__(self, port:str, baudRate:int, listenFreqHZ:float) -> None:
        timeOut = 1/listenFreqHZ
        self.ser = serial.Serial(port, baudRate, timeout=timeOut)
    
    def listen(self) -> str:
        return self.ser.readline().decode().strip()
    
    def send(self, message:str) -> None:
        self.ser.write(message)

    def __del__(self): # mad sketchy
        self.ser.close()
        delattr(self, 'ser')
        
    
