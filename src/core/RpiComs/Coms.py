import RPi.GPIO as GPIO
import os
import serial
from typing import List

def valToBytes(value, mutable=False) -> (bytes | bytearray):
    return bytearray(value) if mutable else bytes(value)

class Sender(object):

    def __init__(self, pins:List[int]) -> None:
        self.pins = pins
        self.dataPins = pins[:(len(pins) - (len(pins)%8))]
        self.syncPins = pins[(len(pins) - (len(pins)%8)):]
    

    def send(self,value):
        pass

    def sendByte(self, X:(bytearray | bytes)):
        pass

class Listener(object):

    def __init__(self, pins:List[int]) -> None:
        self.pins = pins
        