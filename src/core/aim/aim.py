'''
 Copyright (c) 2024 Dhairya Sarvaiya @Byte717

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

2. Any works created using this Software or any modified version of this Software must include attribution to the original software author(s). Attribution should include the following information: 
    a. The name of the software.

    b. The name of the original author(s) of the software.

    c. The URL or other reference to the software (Github).

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

'''

from math import  pi, cos, sin, sqrt


class Trajectory(object):

    def __init__(self, velocity:float,lowAngle:float, highAngle:float, airResistance:float=None) -> None:
        '''
        @param: velocity, shooter velocity in m/s please
        @param: lowest angle of shooter in radians 
        @param: highest angle of shooter in radians
        @param: airResistance drag deceleration in M/s^2, set to none as default
        '''
        self.v, self.low, self.high = velocity,lowAngle,highAngle
        self.air = airResistance if airResistance != 0 else None


    def optimalAngle(self, dx:float, dy:float) -> float:
        '''
        takes in the horizontal and vertical distance to a target
        @param: dx, horizontal distance to target
        @param: dy, vertical distance to target
        return an aiming angle in radians
        '''
        if self.outOfRange(dx,dy):
            return None
            
        low, high, mid = self.low, self.high, None
        while high - low > 1e-15:
            mid = (low+high)/2
            if self.testTheta(mid, dx,dy):
                high = mid
            else:
                low = mid
        return (low + high)/2

    def testTheta(self, theta:float, dx:float, dy:float) -> bool: # brainrot math
        t = (-self.v*cos(theta) + sqrt((self.v**2)*(cos(theta)**2) + 2*self.air*dx))/self.air if self.air else dx/(self.v*cos(theta))
        h = self.v*sin(theta)*t - 4.9*t*t
        return h > dy 

    def optimalDist(self, fixed:float, dy:float) -> float: # gives optimal distance given a fixed theta
        t = (self.v*sin(fixed) - sqrt((self.v**2)*(sin(fixed)**2) - 4*4.9*dy))/9.8
        return self.v*cos(fixed)*t + ((0.5*self.air*t*t) if self.air else 0)

    def setParam(self, param_name, value) -> None:
        if hasattr(self, param_name) and (isinstance(value, (int, float)) or (param_name == 'air' and value == None)):
            setattr(self, param_name, value)
        else:
            raise Exception("Param Doesn't exist or value has wrong type")
    

    def outOfRange(self, dx:float, dy:float) -> bool:
        t = self.v*sin(pi/4)/4.9 # max range we can extract
        return self.v*cos(pi/4)*t + (0.5*self.air*t*t if self.air else 0) < dx

    @staticmethod
    def rads(degrees:float) -> float:
        return degrees*(pi/180)
