public class Trajectory {
    private double v;
    private double low;
    private double high;
    private Double air;

    public Trajectory(double velocity, double lowAngle, double highAngle, Double airResistance) {
        this.v = velocity;
        this.low = lowAngle;
        this.high = highAngle;
        this.air = airResistance != 0 ? airResistance : null;
    }

    public double optimalAngle(double dx, double dy) {
        if (outOfRange(dx, dy)) {
            return Double.NaN;
        }

        double low = this.low, high = this.high, mid = 0;
        while (high - low > 1e-15) {
            mid = (low + high) / 2;
            if (testTheta(mid, dx, dy)) {
                high = mid;
            } else {
                low = mid;
            }
        }
        return (low + high) / 2;
    }

    private boolean testTheta(double theta, double dx, double dy) {
        double t = air != null ? (-v * Math.cos(theta) + Math.sqrt((v * v) * (Math.cos(theta) * Math.cos(theta)) + 2 * air * dx)) / air : dx / (v * Math.cos(theta));
        double h = v * Math.sin(theta) * t - 4.9 * t * t;
        return h > dy;
    }

    public double optimalDist(double fixed, double dy) {
        double t = (v * Math.sin(fixed) - Math.sqrt((v * v) * (Math.sin(fixed) * Math.sin(fixed)) - 4 * 4.9 * dy)) / 9.8;
        return v * Math.cos(fixed) * t + ((air != null ? 0.5 * air * t * t : 0));
    }

    public void setParam(String paramName, double value) {
        try {
            Trajectory.class.getDeclaredField(paramName).set(this, value);
        } catch (Exception e) {
            throw new RuntimeException("Param Doesn't exist or value has wrong type");
        }
    }

    private boolean outOfRange(double dx, double dy) {
        double t = v * Math.sin(Math.PI / 4) / 4.9; // max range we can extract
        return v * Math.cos(Math.PI / 4) * t + (air != null ? 0.5 * air * t * t : 0) < dx;
    }

    public static double rads(double degrees) {
        return degrees * (Math.PI / 180);
    }
}