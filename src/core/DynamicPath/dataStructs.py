from collections import deque

class Queue:
    '''
    Thread-safe, memory-efficient, maximally-sized queue supporting queueing and
    dequeueing in worst-case O(1) time.
    '''
    def __init__(self, max_size = 1e6):
        '''
        Initialize this queue to the empty queue.

        Parameters
        ----------
        max_size : int
            Maximum number of items contained in this queue. Defaults to 10.
        '''
        self._queue = deque(maxlen=max_size)

    def push(self, item):
        '''
        Queues the passed item (i.e., pushes this item onto the tail of this
        queue).

        If this queue is already full, the item at the head of this queue
        is silently removed from this queue *before* the passed item is
        queued.
        '''

        self._queue.append(item)


    def pop(self):
        '''
        Dequeues (i.e., removes) the item at the head of this queue *and*
        returns this item.

        Raises
        ----------
        IndexError
            If this queue is empty.
        '''
        return self._queue.pop()

class DSU:
    # parent:list = []; size : list = []
    def __init__(self,n:int) -> None:
        self.parent = [i for i in range(n)]
        self.size = [1*n]

    def get(self, x:int) -> int:
        while x != self.parent[x]: x = self.parent[x]; return x

    def same_set(self, a: int,b: int) -> bool: return self.get(a) == self.get(b)

    def sizeof(self,x:int) -> int: return self.size[x]

    def link(self,a:int,b:int) -> bool:
        a = self.get(a); b = self.get(b)
        if(a == b):return False
        if(self.size[a] < self.size[b]):
            a, b = b, a
        self.size[a] += self.size[b]
        self.parent[b] = a
        return True
