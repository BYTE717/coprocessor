# using bfs
from typing import List, Tuple, Any
from queue import PriorityQueue
import json
import os
from dataStructs import DSU, Queue

class ShortestPath(object):
    dx, dy = (1,-1,0,0), (0,0,1,-1) # potentilly account for diagonals later
    def __init__(self, map:List[List[bool]]) -> None:
        '''
        @param: map, a 2d boolean array corresponding to a field map with false meaning a blocked coordinate
        '''
        self.map = map
        self.n = len(map)
        self.m = len(map[0])

    def getPath(self, startX:int, startY:int, endX:int, endY:int) -> Tuple[int ,List[Tuple[int,int]]]:
        '''
        given a starting point and ending point, find the shortest path and "block distance"
        '''
        if not self.checkBounds(startX, startY): raise IndexError("Out of bounds ben stokes")
        dist, last = self.__bfs(startX,startY)
        path = [(endX, endY)]
        cur = (endX, endY)
        while cur != (startX, startY):
            cur = last[cur[0]][cur[1]]
            path.append(cur)
        return (dist[endX][endY], reversed(path))

        

    def __bfs(self,x:int, y:int) -> Tuple[List[List[int]], List[List[Tuple[int,int]]]]: # dist grid first, then last grid
        dist = [[-1 for j in range(self.m)] for _ in range(self.n)]
        last = [[(-1,-1) for j in range(self.m)] for _ in range(self.n)]
        q = Queue()
        q.push((x,y))
        dist[x][y] = 0
        while len(q) > 0:
            curX, curY = q.pop()
            for deltaX, deltaY in zip(self.dx, self.dy):
                newX, newY = curX + deltaX, curY + deltaY
                if ((newX >= 0 and newX < self.n and newY >= 0 and newY < self.m) # not out of bound
                    and (dist[newX][newY] > dist[curX][curY] + 1) # dist to this place is shorter than before
                    and self.map[newX][newY] # we can visit this?
                    and dist[newX][newY] == -1): # haven't already accounted for it
                    dist[newX][newY] = dist[curX][curY] + 1
                    last[newX][newY] = (curX, curY)
                    q.push((newX, newY))
        return (dist, last)
    
    def __djikstras(self, x:int, y:int) ->  Tuple[List[List[int]], List[List[Tuple[int,int]]]]:
        raise NotImplementedError("Haven't implement djikstras")
    
    def ___bellmanFord(self, x:int, y:int) ->  Tuple[List[List[int]], List[List[Tuple[int,int]]]]:
        raise NotImplementedError("Haven't integrated Bellman Ford")

    def changeSinglePos(self, x:int, y:int, value:bool) -> None:
        if not self.checkBounds(x, y): raise IndexError("Out of bounds")
        self.map[x][y] = value
    
    def updateMap(self, newMap:List[List[bool]]) -> None:
        self.map = newMap
        self.n = len(newMap)
        self.m = len(newMap[0])
    
    def checkBounds(self, x:int, y:int) -> bool:
        return x >= 0 and x < self.n and y >= 0 and y < self.m

def upscale(map:List[List[Any]], factor:int) -> List[List[Any]]:
    '''
    upscale a map(boolean 2d array) by an integer factor
    '''
    return [[cell for cell in row for _ in range(factor)] for row in map for _ in range(factor)]

class FieldLoader(object): # navgrid file reader and Map objectt
    def __init__(self, fileDir:str) -> None:
        if not os.path.isfile(fileDir):
            raise NotADirectoryError("Where the heck is your navgrid file ben stokes")
        self.f = json.loads(open(fileDir, "r"))
        self.map = self.f["grid"]


if __name__ == '__main__':
    pass