'''
 Copyright (c) 2024 Dhairya Sarvaiya @Byte717

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

2. Any works created using this Software or any modified version of this Software must include attribution to the original software author(s). Attribution should include the following information: 
    a. The name of the software.

    b. The name of the original author(s) of the software.

    c. The URL or other reference to the software (Github).

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''
from structs import Task, TaskGroup
from typing import List
from copy import deepcopy
from operator import attrgetter
from helpers import PQueue
import os
from multiprocessing import Process

class Optimizer(object):
    '''
    Class to optimize the runtime of multiple tasks with a priority
    '''
    COMMAND = "taskset -p -c %d %d"
    def __init__(self, tasks:List[Task], maxProcesses:int=1) -> None:
        '''
        @param tasks is a List of Task objects
        @param maxProcesses is the maximum number of cores to dedicate to running the tasks
        '''
        self.tasks = tasks
        self.m = maxProcesses
        self.n = len(tasks)
        self.min = self.__order()
        self.tasks = sorted(self.tasks, key=attrgetter("priority"))
        if not self.min: self.tasks = reversed(self.tasks)
        self.groups = self.__makeGroups(self.tasks)

    def __order(self) -> bool:
        '''
        Determines wheter to use min or max order
        '''
        cpy = deepcopy(self.tasks)
        minSort = [i.priority for i in sorted(cpy,key=attrgetter("priority"))]
        maxSort = reversed(minSort)
        return self.__timeTaken(minSort) <= self.__timeTaken(maxSort)
    
    def __timeTaken(self, arr:List[float]) -> float:
        '''
        gives total sum of priorities
        '''
        s = PQueue() # pls use multiset instead
        for i in range(self.m): s.push(arr[i])
        for i in range(self.m, self.n):
            curMin = s.pop()
            s.push(curMin+i)
        return s.getMx()
    
    def __makeGroups(self,arr:List[Task]) -> List[TaskGroup]: # slow impl cuz im lazy
        grps = [[i] for i in range(self.m)]
        for i in range(self.m, self.n):
            curMn, mnIdx = -1,-1
            for i, val in enumerate(grps):
                if sum(val) < curMn:
                    curMn = sum(val)
                    mnIdx = i
            grps[mnIdx].append(i)
        return [TaskGroup([arr[i] for i in x]) for x in grps]

        


    def execute(self) -> None:
        '''
        executes the list pf tasks in optimal order
        '''
        processes = []
        for i, val in enumerate(self.groups):
            p = Process(target=val.runTasks)
            os.system(Optimizer.COMMAND % (i,p.pid))
            processes.append(p)
            p.start()
            
        for i in processes: i.join()

