from typing import Callable, Any, Tuple, List

class Task(object):
    def __init__(self, func:Callable[..., Any], args:Tuple[Any]=None, priority:float=None) -> None:
        '''
        @param func is a function to be run (aka a task)
        @param args is the args passed in as a TUPLE
        @param priority is a float with the priority of the function, (aka a mixture of time + complexity, higher means a more complex function)
        '''
        self.func = func
        self.args = args
        self.priority = priority

    def execute(self):
        '''
        this does what you think it does
        '''
        self.func(*self.args) if self.args is not None else self.func()

    def setPriority(self, val:float) -> None:
        self.priority = val

class TaskGroup(object):
    def __init__(self, tasks:List[Task]) -> None:
        '''
        @param tasks is a List of Task objects, not functions,

        You could also do T = TaskGroup([Task(f, None, i) for f, i in zip(funcs, priorities)])
        '''
        self.tasks = tasks
    
    def runTasks(self):
        '''
        this does what you think it does
        '''
        for i in self.tasks:
            i.execute()