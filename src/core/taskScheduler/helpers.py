'''
 Copyright (c) 2024 Dhairya Sarvaiya @Byte717

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

2. Any works created using this Software or any modified version of this Software must include attribution to the original software author(s). Attribution should include the following information: 
    a. The name of the software.

    b. The name of the original author(s) of the software.

    c. The URL or other reference to the software (Github).

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''
import heapq
from typing import Callable, Any, Tuple
import time
import dis
from math import pow, e, log
from queue import PriorityQueue as pq

class PQueue:
    def __init__(self, maxQ=False, attr:str=None) -> None:
        self.q = pq()
        self.maxQ = maxQ
        self.attr = attr

    def push(self,element) -> None:
        if not hasattr(element, self.attr): raise AttributeError("The element doesn't got that attribute")
        if self.maxQ: self.q.put((-getattr(element,self.attr), element))
        else: self.q.put((getattr(element,self.attr), element))
    
    def pop(self):
        return self.q.get()

    def empty(self):
        return self.q._qsize() == 0
    
    def size(self):
        return self.q._qsize()
    
    def getMx(self):
        return max(self.q.queue)

def calcPriority(func:Callable[...,Any],iters=20, args=None, weights:Tuple[float,float]=(0.9,0.1)) -> float:
    # time factor
    avgTime = 0.0
    for _ in range(iters):
        st = time.time()
        func() if args is None else func(*args)
        avgTime += time.time() - st
    avgTime /= iters

    operations = 0
    for op in dis.get_instructions(func):
        if op.opname in ('ADD', 'SUB', 'MULT', 'DIV', 'MOD'):
            operations += 1

    avgTime *= weights[0]
    operations *= weights[1]
    return avgTime + operations
